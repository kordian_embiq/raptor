#include "delay.h"

void delay(unsigned int delay){
    for(unsigned int i=0; i<delay; i++){
        asm("nop");
    } 
}

//void delay_us(int cycles) {
//    // cycles = delay in usec * 100
//    unsigned int cyc_start, cyc, cyc_diff;
//    __asm__ volatile ("rdcycle %0" : "=r"(cyc_start));
//    do {
//        __asm__ volatile ("rdcycle %0" : "=r"(cyc));
//        cyc_diff = cyc - cyc_start;
//    } while(cyc_diff<cycles);
//}
#include "print.h"

void print_dec_full_range(unsigned int v) // up to 4 294 967 295
{
	if (v >= 4294967295) {
		print("O");
		return;
	}
    else if (v >= 4000000000) { putchar('4'); v -= 4000000000; }
    else if (v >= 3000000000) { putchar('3'); v -= 3000000000; }
    else if (v >= 2000000000) { putchar('2'); v -= 2000000000; }
	else if (v >= 1000000000) { putchar('1'); v -= 1000000000; }
	else putchar('0');


    if (v >= 900000000) { putchar('9'); v -= 900000000; }
    else if (v >= 800000000) { putchar('8'); v -= 800000000; }
    else if (v >= 700000000) { putchar('7'); v -= 700000000; }
    else if (v >= 600000000) { putchar('6'); v -= 600000000; }
    else if (v >= 500000000) { putchar('5'); v -= 500000000; }
    else if (v >= 400000000) { putchar('4'); v -= 400000000; }
    else if (v >= 300000000) { putchar('3'); v -= 300000000; }
    else if (v >= 200000000) { putchar('2'); v -= 200000000; }
	else if (v >= 100000000) { putchar('1'); v -= 100000000; }
	else putchar('0');

    if (v >= 90000000) { putchar('9'); v -= 90000000; }
    else if (v >= 80000000) { putchar('8'); v -= 80000000; }
    else if (v >= 70000000) { putchar('7'); v -= 70000000; }
    else if (v >= 60000000) { putchar('6'); v -= 60000000; }
    else if (v >= 50000000) { putchar('5'); v -= 50000000; }
    else if (v >= 40000000) { putchar('4'); v -= 40000000; }
    else if (v >= 30000000) { putchar('3'); v -= 30000000; }
    else if (v >= 20000000) { putchar('2'); v -= 20000000; }
	else if (v >= 10000000) { putchar('1'); v -= 10000000; }
	else putchar('0');

    if (v >= 9000000) { putchar('9'); v -= 9000000; }
    else if (v >= 8000000) { putchar('8'); v -= 8000000; }
    else if (v >= 7000000) { putchar('7'); v -= 7000000; }
    else if (v >= 6000000) { putchar('6'); v -= 6000000; }
    else if (v >= 5000000) { putchar('5'); v -= 5000000; }
    else if (v >= 4000000) { putchar('4'); v -= 4000000; }
    else if (v >= 3000000) { putchar('3'); v -= 3000000; }
    else if (v >= 2000000) { putchar('2'); v -= 2000000; }
	else if (v >= 1000000) { putchar('1'); v -= 1000000; }
	else putchar('0');

    if (v >= 900000) { putchar('9'); v -= 900000; }
    else if (v >= 800000) { putchar('8'); v -= 800000; }
    else if (v >= 700000) { putchar('7'); v -= 700000; }
    else if (v >= 600000) { putchar('6'); v -= 600000; }
    else if (v >= 500000) { putchar('5'); v -= 500000; }
    else if (v >= 400000) { putchar('4'); v -= 400000; }
    else if (v >= 300000) { putchar('3'); v -= 300000; }
    else if (v >= 200000) { putchar('2'); v -= 200000; }
	else if (v >= 100000) { putchar('1'); v -= 100000; }
	else putchar('0');

    if (v >= 90000) { putchar('9'); v -= 90000; }
    else if (v >= 80000) { putchar('8'); v -= 80000; }
    else if (v >= 70000) { putchar('7'); v -= 70000; }
    else if (v >= 60000) { putchar('6'); v -= 60000; }
    else if (v >= 50000) { putchar('5'); v -= 50000; }
    else if (v >= 40000) { putchar('4'); v -= 40000; }
    else if (v >= 30000) { putchar('3'); v -= 30000; }
    else if (v >= 20000) { putchar('2'); v -= 20000; }
	else if (v >= 10000) { putchar('1'); v -= 10000; }
	else putchar('0');
    
    if (v >= 9000) { putchar('9'); v -= 9000; }
    else if (v >= 8000) { putchar('8'); v -= 8000; }
    else if (v >= 7000) { putchar('7'); v -= 7000; }
    else if (v >= 6000) { putchar('6'); v -= 6000; }
    else if (v >= 5000) { putchar('5'); v -= 5000; }
    else if (v >= 4000) { putchar('4'); v -= 4000; }
    else if (v >= 3000) { putchar('3'); v -= 3000; }
    else if (v >= 2000) { putchar('2'); v -= 2000; }
	else if (v >= 1000) { putchar('1'); v -= 1000; }
	else putchar('0');

	if 	(v >= 900) { putchar('9'); v -= 900; }
	else if	(v >= 800) { putchar('8'); v -= 800; }
	else if	(v >= 700) { putchar('7'); v -= 700; }
	else if	(v >= 600) { putchar('6'); v -= 600; }
	else if	(v >= 500) { putchar('5'); v -= 500; }
	else if	(v >= 400) { putchar('4'); v -= 400; }
	else if	(v >= 300) { putchar('3'); v -= 300; }
	else if	(v >= 200) { putchar('2'); v -= 200; }
	else if	(v >= 100) { putchar('1'); v -= 100; }
	else putchar('0');

	if 	(v >= 90) { putchar('9'); v -= 90; }
	else if	(v >= 80) { putchar('8'); v -= 80; }
	else if	(v >= 70) { putchar('7'); v -= 70; }
	else if	(v >= 60) { putchar('6'); v -= 60; }
	else if	(v >= 50) { putchar('5'); v -= 50; }
	else if	(v >= 40) { putchar('4'); v -= 40; }
	else if	(v >= 30) { putchar('3'); v -= 30; }
	else if	(v >= 20) { putchar('2'); v -= 20; }
	else if	(v >= 10) { putchar('1'); v -= 10; }
	else putchar('0');

	if 	(v >= 9) { putchar('9'); v -= 9; }
	else if	(v >= 8) { putchar('8'); v -= 8; }
	else if	(v >= 7) { putchar('7'); v -= 7; }
	else if	(v >= 6) { putchar('6'); v -= 6; }
	else if	(v >= 5) { putchar('5'); v -= 5; }
	else if	(v >= 4) { putchar('4'); v -= 4; }
	else if	(v >= 3) { putchar('3'); v -= 3; }
	else if	(v >= 2) { putchar('2'); v -= 2; }
	else if	(v >= 1) { putchar('1'); v -= 1; }
	else putchar('0');
}
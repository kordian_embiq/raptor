#define     LEDS_OFF    0x0000
#define     LED_1       0x0001
#define     LED_2       0x0002
#define     LED_3       0x0004
#define     LED_4       0x0008  

// led function usage example: LED_1|LED_2
void led(int x);